
public class Ship {

	private Location middlePoint;
	private boolean isDown;
	private boolean isInverted;
	
	public Ship(int x, int y, boolean isInverted)
	{
		this.middlePoint = new Location(x, y);
		this.isInverted = isInverted;
		this.isDown = false;
	}
	
	public boolean getIsDown()
	{
		return isDown;
	}
	
	public void goDown() {
		isDown = true;
	}
	
	public boolean isHit(int x, int y) 
	{
		if(isInverted && middlePoint.x == x)
		{		for(int i = middlePoint.y; i <= middlePoint.y + 2; i = i + 1 )
				if(i == y) 
					return true;
		}
		else if(!isInverted && middlePoint.y == y)
		{
			for(int i = middlePoint.x; i <= middlePoint.x + 2; i = i + 1 )
				if(i == x) 
					return true;
		}
		return false;
	}
}
