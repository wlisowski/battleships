import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LocationTest {

	@Test
	void test() {
		Location a = new Location(1, 1, false);
		
		assertTrue(a.x == 1);
		assertTrue(a.y == 1);
		assertTrue(a.isInverted == false);
	}
	
	@Test
	void test2() {
		Location a = new Location();
		
		assertTrue(a.x == 1);
		assertTrue(a.y == 1);
		assertTrue(a.isInverted == false);
	}

}
