import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ShipTest {

	@Test
	void isNewShipDownTest() {
		Ship ship = new Ship(1, 1, false);
		
		assertTrue(ship.getIsDown() == false);
	}
	
	@Test
	void isHitShipDownTest() {
		Ship ship = new Ship(1, 1, false);
		
		ship.goDown();
		
		assertTrue(ship.getIsDown() == true);
	}
	
	@Test
	void isShipHitInCenterTest() {
		Ship ship = new Ship(1,1,false);
		Ship shipInv = new Ship(1,1,true);
		
		assertTrue(shipInv.isHit(1,1) == true);
		assertTrue(ship.isHit(1,1) == true);
	}

	@Test
	void isShipHitInSideTest() {
		Ship ship = new Ship(1,1,false);
		
		assertTrue(ship.isHit(2,1) == true);	
		assertTrue(ship.isHit(3,1) == true);

		assertTrue(ship.isHit(1, 0) == false);
		assertTrue(ship.isHit(4, 1) == false);
	}
	
	@Test
	void isInvertedShipHitInSideTest() {
		Ship ship = new Ship(1, 1, true);
		
		assertTrue(ship.isHit(1, 2) == true);	
		assertTrue(ship.isHit(1, 3) == true);
	
		assertTrue(ship.isHit(1, 4) == false);
		assertTrue(ship.isHit(0, 1) == false);
	}
	
}
